
/*
 * Auto generated Run-Time-Environment Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'STM32_OTA_V3' 
 * Target:  'STM32_OTA_V3' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "stm32l4xx.h"

/*  MDK-Packs::Data Exchange:JSON:cJSON:1.7.7 */
#define RTE_DataExchange_JSON_cJSON     /* cJSON */


#endif /* RTE_COMPONENTS_H */
